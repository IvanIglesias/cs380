#include <Stdafx.h>

//function to randomize a float between two values
float randFloat(float a, float b) {
	float random = ((float)rand()) / (float)RAND_MAX;
	float diff = b - a;
	float r = random * diff;
	return a + r;
}
float map_value(float x, float in_min, float in_max, float out_min, float out_max);

DNA::DNA()
{
	
}
//Creation of the DNA of each individual
void DNA::create_dna()
{
	for (int i = 0; i < g_terrain.generation_frame_time; i++)
	{
		genes.push_back(D3DXVECTOR2(randFloat(min, max), randFloat(min, max)));
		//scale vectors
		genes[i] *= randFloat(0.0f, maxforce);
	}
}
//this function takes part of the parent of on chill and part from other and makes the
//dna of the children
DNA DNA::crossover(DNA partner)
{
	DNA child;
	unsigned midpoint = rand() % genes.size();

	child.genes.resize(g_terrain.generation_frame_time);
	for (unsigned i = 0; i < genes.size(); i++)
	{
		if (i > midpoint)
			child.genes[i] = genes[i];
		else
			child.genes[i] = partner.genes[i];
	}
	return child;
}
//This function mutates the dna of the children
//check the ratio with all the genes
void DNA::mutate()
{
	for (unsigned i = 0; i < genes.size(); i++)
	{
		if (randFloat(0, 1) < g_terrain.mutation_rate)
		{
			//mute a new random vector
			genes[i] = D3DXVECTOR2(randFloat(min, max), randFloat(min, max));
			//scale vectors
			genes[i] *= randFloat(0.0f, maxforce);
		}
	}
}
//Create the Rocket information
Individual::Individual(D3DXVECTOR2 loc, D3DXVECTOR2 vel, D3DXVECTOR2 acc, D3DXVECTOR2 targ, bool create_dna)
{
	if (create_dna)
		dna.create_dna();
	location = loc;
	//velocity = vel;
	acceleration = acc;
	target = targ;
	D3DXVECTOR3 targ_pos(target.x, 0, target.y);
	g_terrain.GetRowColumn(&targ_pos, &target_row, &target_col);
}
//add the forces to move the rocket in a direcition
void Individual::applyForces(D3DXVECTOR2 force)
{
	acceleration += force;
}
//maps a float between two values
float map_value(float x, float in_min, float in_max, float out_min, float out_max)
{
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
//updates the position of the rocket
void Individual::update()
{
	velocity += acceleration;
	location += velocity;
	acceleration *= 0;

	auto vec = target - location;
	float d = D3DXVec2Length(&vec);

	//Check the closest distance to target
	if (d < close_distance)
		close_distance = d;
	
	D3DXVECTOR3 pos_actu(location.x, 0, location.y);
	int indiv_row, indiv_col;
	g_terrain.GetRowColumn(&pos_actu, &indiv_row, &indiv_col);

	if ((indiv_row == target_row && indiv_col == target_col)
		|| (indiv_row == target_row + 1 && indiv_col == target_col)
		|| (indiv_row == target_row - 1 && indiv_col == target_col)
		|| (indiv_row == target_row + 1 && indiv_col == target_col + 1)
		|| (indiv_row == target_row - 1 && indiv_col == target_col - 1)
		|| (indiv_row == target_row + 1 && indiv_col == target_col - 1)
		|| (indiv_row == target_row - 1 && indiv_col == target_col + 1)
		|| (indiv_row == target_row  && indiv_col == target_col + 1)
		|| (indiv_row == target_row  && indiv_col == target_col - 1))
	{
		reach_target = true;
		g_terrain.path_found = true;
	}
	else if (!reach_target)
		frame_time++;

}
//computes the fitness of each rocket
void Individual::compute_fitness()
{

	fitness = pow(1.0f / (close_distance), 2) + 0.01f;

	if (collide && !reach_target)
		fitness /= g_terrain.collision_affection;
	if (reach_target)
	{
		fitness *= g_terrain.reach_target_bonus;
		float fit_val = map_value(static_cast<float>(g_terrain.generation_frame_time - frame_time), 0, static_cast<float>(g_terrain.generation_frame_time), 0, 10.0f);
		fitness *= fit_val;
	}

}
//updates all the information
void Individual::run()
{
	applyForces(dna.genes[geneCounter]);
	geneCounter++;
	update();
}
//function to select by probabilty a parent
Individual& Population::get_one_parent()
{
	float rand_float = randFloat(0, 1);
	unsigned index = 0;

	while (rand_float >= 0)
	{
		if (index >= population.size())
			break;
		rand_float = rand_float - population[index].fitness;
		index++;
		
	}
	index--;

	return population[index];
}


//Selection of the parents for the next generation
void Population::selection()
{
	//compute the fitness of all the individuals
	float total_fitness = 0;
	for (unsigned i = 0; i < population.size(); i++)
	{
		population[i].compute_fitness();

		total_fitness += population[i].fitness;		
	}	
	for (unsigned i = 0; i < population.size(); i++)
	{
		//normalize fitness
		population[i].fitness /= total_fitness;	
	}

	g_terrain.generation_count++;
}
//creates the next generation
void Population::reproduction()
{
	std::vector<Individual> temp_population;

	for (unsigned i = 0; i < population.size(); i++)
	{		
		//choose parents
		DNA partnerA = get_one_parent().dna;
		DNA partnerB = get_one_parent().dna;

		Individual child(g_terrain.location, { 0.001f,0.001f }, { 0,0 }, g_terrain.target, false);
		child.dna = partnerA.crossover(partnerB);

		child.dna.mutate();
		//overwrite population
		temp_population.push_back(child);
	}
	population = temp_population;
}