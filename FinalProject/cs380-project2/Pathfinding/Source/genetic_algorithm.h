//Genetic_algorithm file
#pragma once



class DNA
{	
public:
	std::vector<D3DXVECTOR2> genes;
		
	float maxforce = 0.001f;
	float max = 1.0f;
	float min = -1.0f;

	DNA();

	void create_dna();

	DNA crossover(DNA partner);

	void mutate();
	
};

class Individual
{
public:
	DNA dna; //Rocket dna

	float fitness;	//fitness value for each rocket
	
	bool collide{ false };

	bool reach_target{ false };

	float close_distance = FLT_MAX;

	int frame_time = 0;

	int target_row, target_col;

	D3DXVECTOR2 velocity{ 0.0f, 0.0f };
	D3DXVECTOR2 target { 0.7f, 0.7f };
	D3DXVECTOR2 location{ 0.5f,0.5f };
	D3DXVECTOR2 acceleration;
	Individual(){}

	Individual(D3DXVECTOR2 loc, D3DXVECTOR2 vel, D3DXVECTOR2 acc, D3DXVECTOR2 targ, bool create_dna = true);

	void applyForces(D3DXVECTOR2 force);

	void update();
	void compute_fitness();
	int geneCounter = 0;
	void run();

};

class Population
{
public:
	float mutationRate;
	std::vector<Individual> population;
	int generations = 0;

	std::vector<float> norm_fitness;
	

	Individual &get_one_parent();

	void selection();
	void reproduction();

};
