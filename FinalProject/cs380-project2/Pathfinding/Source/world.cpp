/* Copyright Steve Rabin, 2012. 
 * All rights reserved worldwide.
 *
 * This software is provided "as is" without express or implied
 * warranties. You may freely copy and compile this source into
 * applications you distribute provided that the copyright text
 * below is included in the resulting source code, for example:
 * "Portions Copyright Steve Rabin, 2012"
 */

#include <Stdafx.h>

//#define UNIT_TESTING

Population indiv_popu;
std::vector<GameObject *> agents;


World::World(void)
: m_initialized(false)
{

}

World::~World(void)
{
}

bool CheckPos(int row, int col)
{
	if ((row >= 0) && (row < g_terrain.GetWidth()) &&
		(col >= 0) && (col < g_terrain.GetWidth()))
		return true;
	else
		return false;
}

void World::InitializeSingletons( void )
{
	//Create Singletons
	m_clock = &g_clock;
	m_database = &g_database;
	m_msgroute = &g_msgroute;
	m_debuglog = &g_debuglog;
	m_debugdrawing = &g_debugdrawing;
	m_terrain = &g_terrain;
}

void CreatePopulation()
{
	indiv_popu.population.clear();
	indiv_popu.mutationRate = g_terrain.mutation_rate;

	//Create population	
	for (int i = 0; i < g_terrain.population_number; i++)
	{
		indiv_popu.population.push_back({ g_terrain.location,{ 0.001f,0.001f },{ 0,0 },g_terrain.target });
	}
}

void CreateAgents()
{

	/*for (unsigned i = 0; i < agents.size(); i++)
	{
		agents[i]->MarkForDeletion();
		
	}*/
	agents.clear();
	for (int i = 0; i < g_terrain.max_population_number; i++)
	{
		GameObject* agent = new GameObject(g_database.GetNewObjectID(), OBJECT_Player, "Player");
		D3DXVECTOR3 pos(0.5f, 0.0f, 0.5f);
		agent->CreateBody(100, pos);
		agent->CreateMovement();
		agent->CreateTiny(g_terrain.pMA_temp, g_terrain.pv_pChars_temp, g_terrain.pSM_temp, g_terrain.dTimeCurrent_temp);
		g_database.Store(*agent);
		agent->CreateStateMachineManager();
		agent->GetStateMachineManager()->PushStateMachine(*new Agent(*agent), STATE_MACHINE_QUEUE_0, true);
		agents.push_back(agent);
		
	}
	
}

void ResetAgents()
{
	for (int i = 0; i < g_terrain.max_population_number; i++)
	{
		agents[i]->draw_object = false;
	}

	for (int i = 0; i < g_terrain.population_number; i++)
	{		
		agents[i]->draw_object = true;
		D3DXVECTOR3 position(g_terrain.location.x, 0, g_terrain.location.y);
		agents[i]->GetBody().SetPos(position);
	}
}


void World::Initialize( CMultiAnim *pMA, std::vector< CTiny* > *pv_pChars, CSoundManager *pSM, double dTimeCurrent )
{
	if(!m_initialized)
	{
		m_initialized = true;
	
		g_terrain.Create();


#ifdef UNIT_TESTING

		//Create unit test game objects
		GameObject* unittest1 = new GameObject( g_database.GetNewObjectID(), OBJECT_Ignore_Type, "UnitTest1" );
		GameObject* unittest2 = new GameObject( g_database.GetNewObjectID(), OBJECT_Ignore_Type, "UnitTest2" );
		GameObject* unittest3a = new GameObject( g_database.GetNewObjectID(), OBJECT_Ignore_Type, "UnitTest3a" );
		GameObject* unittest3b = new GameObject( g_database.GetNewObjectID(), OBJECT_Ignore_Type, "UnitTest3b" );
		GameObject* unittest4 = new GameObject( g_database.GetNewObjectID(), OBJECT_Ignore_Type, "UnitTest4" );
		GameObject* unittest5 = new GameObject( g_database.GetNewObjectID(), OBJECT_Ignore_Type, "UnitTest5" );
		GameObject* unittest6 = new GameObject( g_database.GetNewObjectID(), OBJECT_Ignore_Type, "UnitTest6" );
		
		D3DXVECTOR3 pos(0.0f, 0.0f, 0.0f);
		unittest1->CreateBody( 100, pos );
		unittest2->CreateBody( 100, pos );
		unittest3a->CreateBody( 100, pos );
		unittest3b->CreateBody( 100, pos );
		unittest4->CreateBody( 100, pos );
		unittest5->CreateBody( 100, pos );
		unittest6->CreateBody( 100, pos );

		unittest1->CreateStateMachineManager();
		unittest2->CreateStateMachineManager();
		unittest3a->CreateStateMachineManager();
		unittest3b->CreateStateMachineManager();
		unittest4->CreateStateMachineManager();
		unittest5->CreateStateMachineManager();
		unittest6->CreateStateMachineManager();
		
		g_database.Store( *unittest1 );
		g_database.Store( *unittest2 );
		g_database.Store( *unittest3a );
		g_database.Store( *unittest3b );
		g_database.Store( *unittest4 );
		g_database.Store( *unittest5 );
		g_database.Store( *unittest6 );

		//Give the unit test game objects a state machine
		unittest1->GetStateMachineManager()->PushStateMachine( *new UnitTest1( *unittest1 ), STATE_MACHINE_QUEUE_0, TRUE );
		unittest2->GetStateMachineManager()->PushStateMachine( *new UnitTest2a( *unittest2 ), STATE_MACHINE_QUEUE_0, TRUE );
		unittest3a->GetStateMachineManager()->PushStateMachine( *new UnitTest3a( *unittest3a ), STATE_MACHINE_QUEUE_0, TRUE );
		unittest3b->GetStateMachineManager()->PushStateMachine( *new UnitTest3b( *unittest3b ), STATE_MACHINE_QUEUE_0, TRUE );
		unittest4->GetStateMachineManager()->PushStateMachine( *new UnitTest4( *unittest4 ), STATE_MACHINE_QUEUE_0, TRUE );
		unittest5->GetStateMachineManager()->PushStateMachine( *new UnitTest5( *unittest5 ), STATE_MACHINE_QUEUE_0, TRUE );
		unittest6->GetStateMachineManager()->PushStateMachine( *new UnitTest6( *unittest6 ), STATE_MACHINE_QUEUE_0, TRUE );

#else

	//take variables
	
		g_terrain.pMA_temp = pMA;
		g_terrain.pv_pChars_temp = pv_pChars;
		g_terrain.pSM_temp = pSM;
		g_terrain.dTimeCurrent_temp = dTimeCurrent;


	{	//Agent
			
			CreateAgents();

			//target agent
			GameObject* agent = new GameObject(g_database.GetNewObjectID(), OBJECT_Player, "Player");
			D3DXVECTOR3 pos(g_terrain.target.x, 0.0f, g_terrain.target.y);
			agent->CreateBody(100, pos);
			agent->CreateMovement();
			agent->CreateTiny(pMA, pv_pChars, pSM, dTimeCurrent);
			agent->GetTiny().SetDiffuse(1.0f, 0.0f, 0.0f);
			g_database.Store(*agent);
			agent->CreateStateMachineManager();
			agent->GetStateMachineManager()->PushStateMachine(*new Agent(*agent), STATE_MACHINE_QUEUE_0, true);
			g_terrain.target_agent = agent;

	}

#if defined (PROJECT_THREE)
	{	// Enemy
		GameObject* enemy = new GameObject(g_database.GetNewObjectID(), OBJECT_Enemy, "Enemy");
		D3DXVECTOR3 pos(0.5f, 0.0f, 0.5f);
		enemy->CreateBody(100, pos);
		enemy->CreateMovement();
		enemy->GetMovement().SetSingleStep(false);
		enemy->GetMovement().SetDebugDraw(false);
		enemy->CreateTiny(pMA, pv_pChars, pSM, dTimeCurrent);
		enemy->GetTiny().SetDiffuse(0.0f, 0.0f, 1.0f);
		g_database.Store(*enemy);
		enemy->CreateStateMachineManager();
		enemy->GetStateMachineManager()->PushStateMachine(*new Enemy(*enemy), STATE_MACHINE_QUEUE_0, true);
	}
#endif

	CreatePopulation();


#endif

	}
}


void World::PostInitialize()
{
	g_database.Initialize();
}

int frames_counter = 0;
void World::Update()
{
	if (!g_terrain.pause_animation && !g_terrain.reset_animation)
	{
		//Create population
		if (frames_counter < g_terrain.generation_frame_time)
		{
			//update all the rockets positions
			for (unsigned i = 0; i < indiv_popu.population.size(); i++)
			{
				if (indiv_popu.population[i].collide)
					continue;
				indiv_popu.population[i].run();
				auto pos_indiv = indiv_popu.population[i].location;
				D3DXVECTOR3 new_pos(pos_indiv.x, 0, pos_indiv.y);
				int row, column;

				//It has collide with something then we stop 
				if (!g_terrain.GetRowColumn(&new_pos, &row, &column) || g_terrain.IsWall(row, column))
				{
					indiv_popu.population[i].collide = true;
					continue;
				}
				agents[i]->GetBody().SetPos(new_pos);				
			}
			frames_counter++;
		}
		else
		{
			//evolve the next generation
			indiv_popu.selection();
			indiv_popu.reproduction();
			frames_counter = 0;
		}
	}
	else if (g_terrain.create_agents)
	{
		ResetAgents();
		g_terrain.create_agents = false;
		g_terrain.reset_animation = true;
	}
	else if (g_terrain.reset_animation)
	{
		ResetAgents();
		CreatePopulation();
		g_terrain.reset_animation = false;
		g_terrain.text->SetVisible(false);
		g_terrain.generation_count = 0;
		g_terrain.path_found = false;
		frames_counter = 0;
	}
	if (g_terrain.path_found)
		g_terrain.text->SetVisible(true);
	

	g_clock.MarkTimeThisTick();
	g_database.Update();
}

void World::Animate( double dTimeDelta )
{
	g_database.Animate( dTimeDelta );
}

void World::AdvanceTimeAndDraw( IDirect3DDevice9* pd3dDevice, D3DXMATRIX* pViewProj, double dTimeDelta, D3DXVECTOR3 *pvEye )
{
	g_database.AdvanceTimeAndDraw( pd3dDevice, pViewProj, dTimeDelta, pvEye );
	g_terrain.DrawDebugVisualization( pd3dDevice );
}

void World::RestoreDeviceObjects( LPDIRECT3DDEVICE9 pd3dDevice )
{
	return( g_database.RestoreDeviceObjects( pd3dDevice ) );
}

void World::InvalidateDeviceObjects( void )
{
	g_database.InvalidateDeviceObjects();
}
