// Author: Chi-Hao Kuo
// Updated: 12/25/2015

/* Copyright Steve Rabin, 2012. 
 * All rights reserved worldwide.
 *
 * This software is provided "as is" without express or implied
 * warranties. You may freely copy and compile this source into
 * applications you distribute provided that the copyright text
 * below is included in the resulting source code, for example:
 * "Portions Copyright Steve Rabin, 2012"
 */


#include <Stdafx.h>

void Enemy::FieldOfView(float angle, float closeDistance, float occupancyValue)
{
	// TODO: Implement this for the Occupancy Map project.

	// Parameters:
	//   angle - view cone angle in degree
	//   closeDistance - if the tile is within this distance, no need to check angle
	//   occupancyValue - if the tile is in FOV, apply this value

	// first, clear out old occupancyValue from previous frame
	// (mark m_terrainInfluenceMap[r][c] as zero if the old value is negative

	// For every square on the terrain that is within the field of view cone
	// by the enemy square, mark it with occupancyValue.

	// If the tile is close enough to the enemy (less than closeDistance),
	// you only check if it's visible to the enemy.
	// Otherwise you must also consider the direction the enemy is facing:

	// Get enemy's position and direction
	//   D3DXVECTOR3 pos = m_owner->GetBody().GetPos();
	//   D3DXVECTOR3 dir = m_owner->GetBody().GetDir();

	// Give the enemyDir a field of view a tad greater than the angle:
	//   D3DXVECTOR2 enemyDir = D3DXVECTOR2(dir.x, dir.z);

	// Two grid squares are visible to each other if a line between 
	// their centerpoints doesn't intersect the four boundary lines
	// of every walled grid square. Put this code in IsClearPath().

	if (!g_blackboard.GetTerrainAnalysisFlag() ||
		(g_blackboard.GetTerrainAnalysisType() != TerrainAnalysis_HideAndSeek))
		return;


	// WRITE YOUR CODE HERE
	//Clear

	//Clear the influence map
	auto width = g_terrain.GetWidth();
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < width; j++)
		{
			auto val = g_terrain.GetInfluenceMapValue(i, j);
			if(val < 0)
				g_terrain.SetInfluenceMapValue(i, j, 0);
		}
	}
	D3DXVECTOR3 pos = m_owner->GetBody().GetPos();
	D3DXVECTOR3 dir = m_owner->GetBody().GetDir();
	int row_enemy, col_enemy;
	g_terrain.GetRowColumn(&pos, &row_enemy, &col_enemy);
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < width; j++)
		{

			auto grid_pos = g_terrain.GetCoordinates(i, j);
			//compute distance 
			auto dist = sqrt((grid_pos.x - pos.x) * (grid_pos.x - pos.x) +
				(grid_pos.z - pos.z) * (grid_pos.z - pos.z)) * width;
			//Check if a clear path is abailable
			if (!g_terrain.IsClearPath(row_enemy, col_enemy, i, j))
				continue;
			if (closeDistance > dist)
			{
				//check if it's visible to the enemy.				
				g_terrain.InitialOccupancyMap(i, j, occupancyValue);				
			}
			else //consider the direction the enemy is facing
			{
				auto vec_enemy_point = pos - grid_pos;
				//Normalize
				D3DXVECTOR3 dir_normalize, vec_enemy_point_normalize;
				D3DXVec3Normalize(&dir_normalize, &dir);
				D3DXVec3Normalize(&vec_enemy_point_normalize, &vec_enemy_point);

				if (D3DXVec3Dot(&dir_normalize, &vec_enemy_point_normalize) < cosf(D3DXToRadian(angle / 2.0f)))
				{
					g_terrain.InitialOccupancyMap(i, j, occupancyValue);
				}
			}
		}
	}
}

bool Enemy::FindPlayer(void)
{
	// TODO: Implement this for the Occupancy Map project.

	// Check if the player's tile has negative value (FOV cone)
	// Return true if player is within field cone of view of enemy
	// Return false otherwise
	
	// You need to also set new goal to player's position:
	//   ChangeGoal(row_player, col_player);
	
	// WRITE YOUR CODE HERE
	
	auto player_row = g_blackboard.GetRowPlayer();
	auto player_col = g_blackboard.GetColPlayer();
	if (player_row == m_rowGoal && player_col == m_colGoal)
		return true;
	//Change goal if player is on field of view
	if (g_terrain.GetInfluenceMapValue(player_row, player_col) < 0)
	{		
		ChangeGoal(player_row, player_col);
		return true;
	}

	return false;
}

bool Enemy::SeekPlayer(void)
{
	// TODO: Implement this for the Occupancy Map project.

	// Find the tile with 1.0 occupancy value, and set it as 
	// the new goal:
	//   ChangeGoal(row, col);
	// If multiple tiles with 1.0 occupancy, pick the closest
	// tile to the enemy

	// Return false if no tile is found


	// WRITE YOUR CODE HERE
	auto width = g_terrain.GetWidth();
	int close_row = 0;
	int close_col = 0;
	float dist_close = FLT_MAX;
	D3DXVECTOR3 pos = m_owner->GetBody().GetPos();
	int row_enemy, col_enemy;
	g_terrain.GetRowColumn(&pos, &row_enemy, &col_enemy);

	if (row_enemy != m_rowGoal && col_enemy != m_colGoal)
		return true;

	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < width; j++)
		{
			if(g_terrain.GetInfluenceMapValue(i, j) == 1.0f)
			{
				//check distance
				auto grid_pos = g_terrain.GetCoordinates(i, j);
				auto dist = sqrt((grid_pos.x - pos.x) * (grid_pos.x - pos.x) +
					(grid_pos.z - pos.z) * (grid_pos.z - pos.z)) * width;
				//Check closests distance
				if (dist < dist_close)
				{
					dist_close = dist;
					close_row = i;
					close_col = j;
				}			
			}
		}
	}

	if (dist_close != FLT_MAX)
	{
		//Change goal if distance found
		ChangeGoal(close_row, close_col);
		return true;
	}else
		return false;
}
