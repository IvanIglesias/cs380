/******************************************************************************/
/*!
\file		A_star.h
\project	CS380 Project 2
\author		Ivan Iglesias
\summary	Algorithm of A*

Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
*/
/******************************************************************************/
#pragma once
#include <set>


struct grid_cell
{
	//Parent position
	int parent_row{ -1 };
	int parent_col{ -1 };
	//Cost values
	float f_value{ FLT_MAX };
	float g_value{ FLT_MAX };
	float h_value{ FLT_MAX };
};
//Use a pair like a vector for the position
typedef std::pair<int, int> grid_pos;
//Use another pair for the set to order by cost
typedef std::pair<float, std::pair<int, int>> cost_grid_pos;

enum neighbours
{
	top, bot, right, left, top_left, top_right, bot_left, bot_right
};

class AStar
{
private:
	
	float CalculateHeuristics(int row, int col);
	bool CheckPos(int row, int col);
	bool LastPosition(int row, int col);
	void ComputeResultingPath(WaypointList & m_waypointList);
	bool CheckNeighbour(int original_r, int original_c,
		WaypointList & m_waypointList, neighbours position);
	void ClearData();
	void InsertSmothing(std::list<D3DXVECTOR3>::iterator point0, std::list<D3DXVECTOR3>::iterator point1,
		std::list<D3DXVECTOR3>::iterator point2, std::list<D3DXVECTOR3>::iterator point3, WaypointList & m_waypointList);
	
	grid_pos dest;
	grid_pos src;

	grid_cell ** grid_info{ nullptr };
	bool ** close_list{ nullptr };
	std::set<cost_grid_pos> open_list;
	bool single_step = false;
	int width{ 0 };

	float heuristic_weight{ 1.0 };
	int heuristic_option{ 1 };
	bool rubberbanding{ false };
	bool catmull{ false };
	bool debug_draw{ true };

public:
	AStar() {}
	~AStar();
	bool LoopA_Star(WaypointList & m_waypointList);
	void SetSingleStep(bool state) { single_step = state; }
	void Set_src_dst(grid_pos src_in, grid_pos dst_in)
	{
		src = src_in;
		dest = dst_in;
	}
	void InitializeContainers();
	void NewBoard(size_t board_width);
	void SetHeuristicWeight(float weight);
	void SetHeuristicOption(int heuris_option);
	void SetRubberbanding(bool rubberbanding_flag);
	void SetCatmull(bool catmull_flag);	
	void SetDebugDraw(bool debug_draw_lines);
	bool IsStraightLine(int r, int c, int curR, int curC);


};




