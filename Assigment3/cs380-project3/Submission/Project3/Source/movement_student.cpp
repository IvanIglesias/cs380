/* Copyright Steve Rabin, 2012. 
 * All rights reserved worldwide.
 *
 * This software is provided "as is" without express or implied
 * warranties. You may freely copy and compile this source into
 * applications you distribute provided that the copyright text
 * below is included in the resulting source code, for example:
 * "Portions Copyright Steve Rabin, 2012"
 */

#include <Stdafx.h>



bool Movement::ComputePath( int r, int c, bool newRequest )
{
	m_goal = g_terrain.GetCoordinates( r, c );
	m_movementMode = MOVEMENT_WAYPOINT_LIST;
	// project 2: change this flag to true
	bool useAStar = true;
	
	if( useAStar )
	{
		///////////////////////////////////////////////////////////////////////////////////////////////////
		//INSERT YOUR A* CODE HERE
		//1. You should probably make your own A* class.
		//2. You will need to make this function remember the current progress if it preemptively exits.
		//3. Return "true" if the path is complete, otherwise "false".
		///////////////////////////////////////////////////////////////////////////////////////////////////
		
		if (newRequest)
		{
			//Get the starting position of the body
			D3DXVECTOR3 cur = m_owner->GetBody().GetPos();
			int curR, curC;
			//Transform it to row and col
			g_terrain.GetRowColumn(&cur, &curR, &curC);
			//Straight Line Optimization			
			if ((GetStraightlinePath() && g_A_Star.IsStraightLine(r, c, curR, curC))
				|| (curR == r && curC == c))
			{
				D3DXVECTOR3 spot = g_terrain.GetCoordinates(r, c);
				m_waypointList.clear();
				m_waypointList.push_back(spot);
				return true;
			}
			//Create the board and set all the new values
			g_A_Star.NewBoard(g_terrain.GetWidth());
			g_A_Star.Set_src_dst({ curR, curC }, { r, c });
			g_A_Star.InitializeContainers();
			g_A_Star.SetHeuristicWeight(GetHeuristicWeight());
			g_A_Star.SetSingleStep(GetSingleStep());
			g_A_Star.SetHeuristicOption(GetHeuristicCalc());
			g_A_Star.SetRubberbanding(GetRubberbandPath());
			g_A_Star.SetCatmull(GetSmoothPath());
			g_A_Star.SetDebugDraw(GetDebugDraw());



			m_waypointList.clear();
		}
		if (g_A_Star.LoopA_Star(m_waypointList))
			return true;
		else					
			return false;	

	}
	else
	{	
		//Randomly meander toward goal (might get stuck at wall)
		int curR, curC;
		D3DXVECTOR3 cur = m_owner->GetBody().GetPos();
		g_terrain.GetRowColumn( &cur, &curR, &curC );

		m_waypointList.clear();
		m_waypointList.push_back( cur );

		int countdown = 100;
		while( curR != r || curC != c )
		{
			if( countdown-- < 0 ) { break; }

			if( curC == c || (curR != r && rand()%2 == 0) )
			{	//Go in row direction
				int last = curR;
				if( curR < r ) { curR++; }
				else { curR--; }

				if( g_terrain.IsWall( curR, curC ) )
				{
					curR = last;
					continue;
				}
			}
			else
			{	//Go in column direction
				int last = curC;
				if( curC < c ) { curC++; }
				else { curC--; }

				if( g_terrain.IsWall( curR, curC ) )
				{
					curC = last;
					continue;
				}
			}

			D3DXVECTOR3 spot = g_terrain.GetCoordinates( curR, curC );
			m_waypointList.push_back( spot );
			g_terrain.SetColor( curR, curC, DEBUG_COLOR_YELLOW );
		}
		return true;
	}
}
