/* Copyright Steve Rabin, 2012.
* All rights reserved worldwide.
*
* This software is provided "as is" without express or implied
* warranties. You may freely copy and compile this source into
* applications you distribute provided that the copyright text
* below is included in the resulting source code, for example:
* "Portions Copyright Steve Rabin, 2012"
*/

#include <Stdafx.h>


float Terrain::ClosestWall(int row, int col)
{
	// TODO: Helper function for the Terrain Analysis project.

	// For each tile, check the distance with every wall of the map.
	// Return the distance to the closest wall or side.
	
	// WRITE YOUR CODE HERE
	
	auto width = GetWidth();

	float smallest_dist = FLT_MAX;
	auto cord_init = GetCoordinates(row, col);
	for (int i = -1; i < width + 1; i++)
	{
		for (int j = -1; j < width + 1; j++)
		{
			//side walls
			if (i < 0 || j < 0 || i == width || j == width)
			{
				auto test_point = GetCoordinates(i, j);

				float distance = sqrt((cord_init.x - test_point.x) * (cord_init.x - test_point.x) +
					(cord_init.z - test_point.z) * (cord_init.z - test_point.z)) * width;
				if (distance < smallest_dist)
					smallest_dist = distance;
			}
			else if (IsWall(i, j))
			{
				auto test_point = GetCoordinates(i, j);

				float distance = sqrt((cord_init.x - test_point.x) * (cord_init.x - test_point.x) +
					(cord_init.z - test_point.z) * (cord_init.z - test_point.z)) * width;
				if (distance < smallest_dist)
					smallest_dist = distance;

			}
		}
	}
	return smallest_dist;	
}

void Terrain::AnalyzeOpennessClosestWall(void)
{
	// TODO: Implement this for the Terrain Analysis project.

	// Mark every square on the terrain (m_terrainInfluenceMap[r][c]) with
	// the value 1/(d*d), where d is the distance to the closest wall in 
	// row/column grid space.
	// Edges of the map count as walls!

	// WRITE YOUR CODE HERE
	auto width = GetWidth();
		
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < width; j++)
		{
			auto dist_closest = ClosestWall(i, j);
			m_terrainInfluenceMap[i][j] = 1.0f / (dist_closest * dist_closest);
		}
	}


}

void Terrain::AnalyzeVisibility(void)
{
	// TODO: Implement this for the Terrain Analysis project.

	// Mark every square on the terrain (m_terrainInfluenceMap[r][c]) with
	// the number of grid squares (that are visible to the square in question)
	// divided by 160 (a magic number that looks good). Cap the value at 1.0.

	// Two grid squares are visible to each other if a line between 
	// their centerpoints doesn't intersect the four boundary lines
	// of every walled grid square. Put this code in IsClearPath().


	// WRITE YOUR CODE HERE
	auto width = GetWidth();

	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < width; j++)
		{
			if (!IsWall(i,j))
			{
				float count = 0;
				for (int x = 0; x < width; x++)
				{
					for (int y = 0; y < width; y++)
					{
						if (!IsWall(x,y))
						{
							if (IsClearPath(i, j, x, y))
								count++;
						}
					}
				}
				auto influence = count / 160.0f;
				m_terrainInfluenceMap[i][j] = (influence > 1.0f) ? 1.0f : influence;
			}
		}
	}



}
bool CheckPos(int row, int col, int width)
{
	if ((row >= 0) && (row < width) &&
		(col >= 0) && (col < width))
		return true;
	else
		return false;
}

void Terrain::AnalyzeVisibleToPlayer(void)
{
	// TODO: Implement this for the Terrain Analysis project.

	// For every square on the terrain (m_terrainInfluenceMap[r][c])
	// that is visible to the player square, mark it with 1.0.
	// For all non 1.0 squares that are visible to, and next to 1.0 squares,
	// mark them with 0.5. Otherwise, the square should be marked with 0.0.

	// Two grid squares are visible to each other if a line between 
	// their centerpoints doesn't intersect the four boundary lines
	// of every walled grid square. Put this code in IsClearPath().


	// WRITE YOUR CODE HERE
	auto width = GetWidth();

	ResetInfluenceMap();
	//Squares visible to the player

	auto row_player = g_blackboard.GetRowPlayer();
	auto col_player = g_blackboard.GetColPlayer();	

	float count = 0;
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < width; y++)
		{
			if (!IsWall(x, y))
			{
				if (IsClearPath(row_player, col_player, x, y))
				{
					m_terrainInfluenceMap[x][y] = 1.0f;

					//Mark the closer ones to 0.5
					
					//Top
					auto row = x + 1;
					auto col = y;
					if (CheckPos(row, col, width) && m_terrainInfluenceMap[row][col] != 1.0f &&   !IsWall(row, col))
						m_terrainInfluenceMap[row][col] = 0.5f;					
					//Right
					row = x;
					col = y + 1;
					if (CheckPos(row, col, width) && m_terrainInfluenceMap[row][col] != 1.0f &&  !IsWall(row, col))
						m_terrainInfluenceMap[row][col] = 0.5f;
					//Bot
					row = x - 1;
					col = y;
					if (CheckPos(row, col, width) && m_terrainInfluenceMap[row][col] != 1.0f &&  !IsWall(row, col))
						m_terrainInfluenceMap[row][col] = 0.5f;
					//Bot
					row = x;
					col = y - 1;
					if (CheckPos(row, col, width) && m_terrainInfluenceMap[row][col] != 1.0f &&  !IsWall(row, col))
						m_terrainInfluenceMap[row][col] = 0.5f;

				}						

			}
		}
	}
	
			
		
	
}

void Terrain::AnalyzeSearch(void)
{
	// TODO: Implement this for the Terrain Analysis project.

	// For every square on the terrain (m_terrainInfluenceMap[r][c])
	// that is visible by the player square, mark it with 1.0.
	// Otherwise, don't change the value (because it will get
	// decremented with time in the update call).
	// You must consider the direction the player is facing:
	// D3DXVECTOR2 playerDir = D3DXVECTOR2(m_dirPlayer.x, m_dirPlayer.z)
	// Give the player a field of view a tad greater than 180 degrees.

	// Two grid squares are visible to each other if a line between 
	// their centerpoints doesn't intersect the four boundary lines
	// of every walled grid square. Put this code in IsClearPath().


	// WRITE YOUR CODE HERE
	auto width = GetWidth();


	//Squares visible to the player
	auto playerDir = D3DXVECTOR2(m_dirPlayer.x, m_dirPlayer.z);
	auto row_player = g_blackboard.GetRowPlayer();
	auto col_player = g_blackboard.GetColPlayer();

	auto pos_player = GetCoordinates(row_player, col_player);


	float count = 0;
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < width; y++)
		{
			if (!IsWall(x, y))
			{
				auto pos_grid = GetCoordinates(x, y);

				//Vector between grid_pos and player
				auto vec_player_grid = D3DXVECTOR2(pos_grid.x - pos_player.x, pos_grid.z - pos_player.z);
				D3DXVec2Dot(&vec_player_grid, &playerDir);
				if (D3DXVec2Dot(&vec_player_grid, &playerDir) >= -0.01f && IsClearPath(row_player, col_player, x, y))
				{
					m_terrainInfluenceMap[x][y] = 1.0f;
				}
			}
		}
	}
}

bool Terrain::IsClearPath(int r0, int c0, int r1, int c1)
{
	// TODO: Implement this for the Terrain Analysis project.

	// Two grid squares (r0,c0) and (r1,c1) are visible to each other 
	// if a line between their centerpoints doesn't intersect the four 
	// boundary lines of every walled grid square. Make use of the
	// function LineIntersect(). You should also puff out the four
	// boundary lines by a very tiny bit so that a diagonal line passing
	// by the corner will intersect it.


	// WRITE YOUR CODE HERE
	//Get center of the grid squares 
	auto grid_center_1 = GetCoordinates(r0, c0);
	auto grid_center_2 = GetCoordinates(r1, c1);

	if (IsWall(r0, c0) || IsWall(r1, c1))
		return false;

	float width = static_cast<float>(GetWidth());
	auto grid_square_width_half = (1.0f / width) * 0.5f;
	auto epsilon = grid_square_width_half / 10.0f;

	//create box 
	//take the smallest and bigger values 
	int min_row, max_row, min_col, max_col;

	min_row = (r0 > r1) ? r1 : r0;
	max_row = (r0 > r1) ? r0 : r1;
	min_col = (c0 > c1) ? c1 : c0;
	max_col = (c0 > c1) ? c0 : c1;

	for (int i = min_row; i <= max_row; i++)
	{
		for (int j = min_col; j <= max_col; j++)
		{
			if (IsWall(i,j))
			{
				//compute four points of the square
				auto check_center = GetCoordinates(i, j);

				auto top_left = D3DXVECTOR2(check_center.x - grid_square_width_half, check_center.z + grid_square_width_half);
				auto top_right = D3DXVECTOR2(check_center.x + grid_square_width_half, check_center.z + grid_square_width_half);
				auto bot_right = D3DXVECTOR2(check_center.x + grid_square_width_half, check_center.z - grid_square_width_half);
				auto bot_left = D3DXVECTOR2(check_center.x - grid_square_width_half, check_center.z - grid_square_width_half);

				//Compute intersection of line with all the lines
				//if not clear path return false
				//top_left top_rigth
				if (LineIntersect(grid_center_1.x, grid_center_1.z, grid_center_2.x, grid_center_2.z,
					top_left.x - epsilon, top_left.y, top_right.x + epsilon, top_right.y))
					return false;
				// top_right bot_right
				if (LineIntersect(grid_center_1.x, grid_center_1.z, grid_center_2.x, grid_center_2.z,
					top_right.x, top_right.y + epsilon, bot_right.x, bot_right.y - epsilon))
					return false;
				// bot_right bot_left
				if (LineIntersect(grid_center_1.x, grid_center_1.z, grid_center_2.x, grid_center_2.z,
					bot_right.x + epsilon, bot_right.y, bot_left.x - epsilon, bot_left.y))
					return false;
				// bot_left top_left
				if (LineIntersect(grid_center_1.x, grid_center_1.z, grid_center_2.x, grid_center_2.z,
					bot_left.x, bot_left.y - epsilon, top_left.x, top_left.y + epsilon))
					return false;
			}
		}
	}
	return true;	
}

void Terrain::Propagation(float decay, float growing, bool computeNegativeInfluence)
{
	// TODO: Implement this for the Occupancy Map project.

	// computeNegativeInfluence flag is true if we need to handle two agents
	// (have both positive and negative influence)
	// computeNegativeInfluence flag is false if we only deal with positive
	// influence (we should ignore all negative influence)

	// Pseudo code:
	//
	// For each tile on the map
	//
	//   Get the influence value of each neighbor after decay
	//   Then keep the decayed influence WITH THE HIGHEST ABSOLUTE.
	//
	//   Apply linear interpolation to the influence value of the tile, 
	//   and the maximum decayed influence value from all neighbors, with growing 
	//   factor as coefficient
	//
	//   Store the result to the temp layer
	//
	// Store influence value from temp layer

	// WRITE YOUR CODE HERE

	auto width = GetWidth();

	//create temporal layer
	float** templayer = new float*[width];
	for (int i = 0; i < width; i++)
		templayer[i] = new float[width] {0};

	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < width; y++)
		{
			if (!IsWall(x, y))
			{
				float max_decayed = 0.0f;

				auto origin_point = GetCoordinates(x, y);

				//Iterate all the neightbours
				std::vector<int> rows = { x + 1, x + 1, x + 1 , x , x - 1, x - 1,  x - 1, x };

				std::vector<int> columns = { y , y - 1 , y + 1 , y + 1 , y , y - 1 , y + 1 , y - 1  };
				for (unsigned i = 0; i < rows.size(); i++)
				{					
					if (CheckPos(rows[i], columns[i], width) && IsClearPath(x, y, rows[i], columns[i]))
					{

						auto test_point = GetCoordinates(rows[i], columns[i]);
						auto vec = test_point - origin_point;
						auto distance = D3DXVec3Length(&vec) * width;
						//   Get the influence value of each neighbor after decay
						auto decayed_val = m_terrainInfluenceMap[rows[i]][columns[i]] * exp(-1 * distance * decay);
						if (computeNegativeInfluence)
						{
							if (fabsf(decayed_val) > fabsf(max_decayed))
								max_decayed = decayed_val;
						}
						else
						{
							if (decayed_val > max_decayed)
								max_decayed = decayed_val;
						}
					}
				}			
				//If compute negative is not active we dont lerp the value
				if (!computeNegativeInfluence && m_terrainInfluenceMap[x][y] < 0)
				{
					templayer[x][y] = m_terrainInfluenceMap[x][y];
				}
				else
					templayer[x][y] = (1.0f - growing) * m_terrainInfluenceMap[x][y] + growing * max_decayed;
			}
		}
	}
	//Copy back all the influence map
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < width; y++)
		{
			m_terrainInfluenceMap[x][y] = templayer[x][y];
		}
	}

	for (int i = 0; i < width; ++i) {
		delete[] templayer[i];
	}
	delete[] templayer;
}

void Terrain::NormalizeOccupancyMap(bool computeNegativeInfluence)
{
	// TODO: Implement this for the Occupancy Map project.

	// divide all tiles with maximum influence value, so the range of the
	// influence is kept in [0,1]
	// if we need to handle negative influence value, divide all positive
	// tiles with maximum influence value, and all negative tiles with
	// minimum influence value * -1, so the range of the influence is kept
	// at [-1,1]
	
	// computeNegativeInfluence flag is true if we need to handle two agents
	// (have both positive and negative influence)
	// computeNegativeInfluence flag is false if we only deal with positive
	// influence, ignore negative influence 

	// WRITE YOUR CODE HERE
	auto width = GetWidth();
	float max_influ = 0;
	float min_influ = 0;
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < width; y++)
		{
			//find max and min influences
			max_influ = m_terrainInfluenceMap[x][y] > max_influ ? m_terrainInfluenceMap[x][y] : max_influ;
			min_influ = m_terrainInfluenceMap[x][y] < min_influ ? m_terrainInfluenceMap[x][y] : min_influ;
		}
	}

	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < width; y++)
		{
			//Apply min and max influences
			if (m_terrainInfluenceMap[x][y] < 0.0)
			{
				if (computeNegativeInfluence && min_influ < 0.0f)
				{
					InitialOccupancyMap(x, y, m_terrainInfluenceMap[x][y] / -min_influ);
				}
			}
			else if(max_influ > 0.0f)
			{
				InitialOccupancyMap(x, y , m_terrainInfluenceMap[x][y] / max_influ);
			}			
		}
	}

}
