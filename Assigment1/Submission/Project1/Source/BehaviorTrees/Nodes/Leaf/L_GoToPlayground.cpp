/******************************************************************************/
/*!
\file		L_MoveToTeacherRow.cpp
\project	CS380/CS580 AI Framework
\author		Chi-Hao Kuo
\summary	Action: Move to a random point.

Copyright (C) 2016 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
*/
/******************************************************************************/

#include <Stdafx.h>

using namespace BT;



/*--------------------------------------------------------------------------*
Name:           OnInitial

Description:    Only run when initializing the node.

Arguments:      nodedata_ptr:	current node data pointer.

Returns:        None.
*---------------------------------------------------------------------------*/
void L_GoToPlayground::OnInitial(NodeData *nodedata_ptr)
{
	LeafNode::OnInitial(nodedata_ptr);
}

/*--------------------------------------------------------------------------*
Name:           OnEnter

Description:    Only run when entering the node.

Arguments:      nodedata_ptr:	current node data pointer.

Returns:        Status:			return status.
*---------------------------------------------------------------------------*/
Status L_GoToPlayground::OnEnter(NodeData *nodedata_ptr)
{
	LeafNode::OnEnter(nodedata_ptr);

	TinyBlackBoard *tinybb = nodedata_ptr->GetAgentData().GetLocalBlackBoard<TinyBlackBoard>();
	GameObject *self = nodedata_ptr->GetAgentData().GetGameObject();
	//all the children in the line
	switch (tinybb->my_playground)
	{
	case 0:
		self->SetTargetPOS(D3DXVECTOR3(0, 0, 0));
		self->SetSpeedStatus(TinySpeedStatus::TS_JOG);
		SetTinySpeed(self);
		tinybb->my_playground = -1;
		break;
	case 1:
		self->SetTargetPOS(D3DXVECTOR3(1, 0, 1));
		self->SetSpeedStatus(TinySpeedStatus::TS_JOG);
		SetTinySpeed(self);
		tinybb->my_playground = -1;

		break;
	case 2:
		self->SetTargetPOS(D3DXVECTOR3(1, 0, 0));
		self->SetSpeedStatus(TinySpeedStatus::TS_JOG);
		SetTinySpeed(self);
		tinybb->my_playground = -1;

		break;
	case 3:
		self->SetTargetPOS(D3DXVECTOR3(0, 0, 1));
		self->SetSpeedStatus(TinySpeedStatus::TS_JOG);
		SetTinySpeed(self);
		tinybb->my_playground = -1;

		break;
	default:
		return Status::BT_FAILURE;
		break;
	}
	
	return Status::BT_RUNNING;	

}

/*--------------------------------------------------------------------------*
Name:           OnExit

Description:    Only run when exiting the node.

Arguments:      nodedata_ptr:	current node data pointer.

Returns:        None.
*---------------------------------------------------------------------------*/
void L_GoToPlayground::OnExit(NodeData *nodedata_ptr)
{
	LeafNode::OnExit(nodedata_ptr);
}

/*--------------------------------------------------------------------------*
Name:           OnUpdate

Description:    Run every frame.

Arguments:      dt:				delta time.
nodedata_ptr:	current node data pointer.

Returns:        Status:			return status.
*---------------------------------------------------------------------------*/
Status L_GoToPlayground::OnUpdate(float dt, NodeData *nodedata_ptr)
{
	LeafNode::OnUpdate(dt, nodedata_ptr);

	GameObject *self = nodedata_ptr->GetAgentData().GetGameObject();

	if (IsNear(self->GetBody().GetPos(), self->GetTargetPOS()))
	{		
		self->SetSpeedStatus(TinySpeedStatus::TS_IDLE);
		SetTinySpeed(self);
		return Status::BT_SUCCESS;
	}
	return Status::BT_RUNNING;
}
/*--------------------------------------------------------------------------*
Name:           OnSuspend

Description:    Only run when node is in suspended.

Arguments:      nodedata_ptr:	current node data pointer.

Returns:        Status:			return status.
*---------------------------------------------------------------------------*/
Status L_GoToPlayground::OnSuspend(NodeData *nodedata_ptr)
{
	return LeafNode::OnSuspend(nodedata_ptr);
}