/******************************************************************************/
/*!
\file		D_Delay.cpp
\project	CS380/CS580 AI Framework
\author		Chi-Hao Kuo
\summary	Suspend Child For 1 To 2 Seconds.

Copyright (C) 2016 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
*/
/******************************************************************************/

#include <Stdafx.h>

using namespace BT;

/* public methods */

/*--------------------------------------------------------------------------*
Name:           GetLocalBlackBoard

Description:    Get custom data pointer.

Arguments:      None.

Returns:        D_DelayData*:	custom node data pointer.
*---------------------------------------------------------------------------*/
D_RunOnceData *D_RunOnce::GetLocalBlackBoard(NodeData *nodedata_ptr)
{
	return nodedata_ptr->GetLocalBlackBoard<D_RunOnceData>();
}

/*--------------------------------------------------------------------------*
Name:           InitialLocalBlackBoard

Description:    Initial custom data.

Arguments:      None.

Returns:        None.
*---------------------------------------------------------------------------*/
void D_RunOnce::InitialLocalBlackBoard(NodeData *nodedata_ptr)
{
	nodedata_ptr->InitialLocalBlackBoard<D_RunOnceData>();
}
/* protected methods */

/*--------------------------------------------------------------------------*
Name:           OnInitial

Description:    Only run when initializing the node.

Arguments:      nodedata_ptr:	current node data pointer.

Returns:        None.
*---------------------------------------------------------------------------*/
void D_RunOnce::OnInitial(NodeData *nodedata_ptr)
{
	InterrupterNode::OnInitial(nodedata_ptr);

	InitialLocalBlackBoard(nodedata_ptr);
}

/*--------------------------------------------------------------------------*
Name:           OnEnter

Description:    Only run when entering the node.

Arguments:      nodedata_ptr:	current node data pointer.

Returns:        Status:			return status.
*---------------------------------------------------------------------------*/
Status D_RunOnce::OnEnter(NodeData *nodedata_ptr)
{

	// run child

	InterrupterNode::OnEnter(nodedata_ptr);	
	
	// tiny idle

	AgentBTData &data = nodedata_ptr->GetAgentData();
	GameObject *self = data.GetGameObject();
	if (self)
	{
		self->SetSpeedStatus(TinySpeedStatus::TS_IDLE);
		SetTinySpeed(self);
	}

	auto local_black = GetLocalBlackBoard(nodedata_ptr);

	//return Status::BT_RUNNING;

	nodedata_ptr->SetChildStatus(0, Status::BT_FAILURE);

	if (!local_black->already_run)
	{
		return Status::BT_RUNNING;
	}
	else 
		return Status::BT_FAILURE;

}

/*--------------------------------------------------------------------------*
Name:           OnExit

Description:    Only run when exiting the node.

Arguments:      nodedata_ptr:	current node data pointer.

Returns:        None.
*---------------------------------------------------------------------------*/
void D_RunOnce::OnExit(NodeData *nodedata_ptr)
{
	//InterrupterNode::OnExit(nodedata_ptr);
}

/*--------------------------------------------------------------------------*
Name:           OnUpdate

Description:    Run every frame.

Arguments:      dt:				delta time.
				nodedata_ptr:	current node data pointer.

Returns:        Status:			return status.
*---------------------------------------------------------------------------*/
Status D_RunOnce::OnUpdate(float dt, NodeData *nodedata_ptr)
{
	Status child_status = nodedata_ptr->GetChildStatus(0);
	TinyBlackBoard *tinybb = nodedata_ptr->GetAgentData().GetLocalBlackBoard<TinyBlackBoard>();
	auto local_black = GetLocalBlackBoard(nodedata_ptr);

	if (child_status != Status::BT_RUNNING)
	{
		if (!local_black->already_run)
		{
			// resume child
			ResetChildReturnStatus(nodedata_ptr);
			RunChild(true, false, nodedata_ptr);	
			local_black->already_run = true;
			return Status::BT_RUNNING;
		}
		return Status::BT_FAILURE;
	}
	else
		return child_status;	
}

/*--------------------------------------------------------------------------*
Name:           OnSuspend

Description:    Only run when node is in suspended.

Arguments:      nodedata_ptr:	current node data pointer.

Returns:        Status:			return status.
*---------------------------------------------------------------------------*/
Status D_RunOnce::OnSuspend(NodeData *nodedata_ptr)
{
	return InterrupterNode::OnSuspend(nodedata_ptr);
}