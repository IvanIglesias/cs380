/******************************************************************************/
/*!
\file		D_Delay.cpp
\project	CS380/CS580 AI Framework
\author		Chi-Hao Kuo
\summary	Suspend Child For 1 To 2 Seconds.

Copyright (C) 2016 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
*/
/******************************************************************************/

#include <Stdafx.h>

using namespace BT;

/* public methods */

/*--------------------------------------------------------------------------*
Name:           GetLocalBlackBoard

Description:    Get custom data pointer.

Arguments:      None.

Returns:        D_DelayData*:	custom node data pointer.
*---------------------------------------------------------------------------*/
D_IfData *D_If::GetLocalBlackBoard(NodeData *nodedata_ptr)
{
	return nodedata_ptr->GetLocalBlackBoard<D_IfData>();
}

/*--------------------------------------------------------------------------*
Name:           InitialLocalBlackBoard

Description:    Initial custom data.

Arguments:      None.

Returns:        None.
*---------------------------------------------------------------------------*/
void D_If::InitialLocalBlackBoard(NodeData *nodedata_ptr)
{
	nodedata_ptr->InitialLocalBlackBoard<D_DelayData>();
}
/* protected methods */

/*--------------------------------------------------------------------------*
Name:           OnInitial

Description:    Only run when initializing the node.

Arguments:      nodedata_ptr:	current node data pointer.

Returns:        None.
*---------------------------------------------------------------------------*/
void D_If::OnInitial(NodeData *nodedata_ptr)
{
	InterrupterNode::OnInitial(nodedata_ptr);

	InitialLocalBlackBoard(nodedata_ptr);
}

/*--------------------------------------------------------------------------*
Name:           OnEnter

Description:    Only run when entering the node.

Arguments:      nodedata_ptr:	current node data pointer.

Returns:        Status:			return status.
*---------------------------------------------------------------------------*/
Status D_If::OnEnter(NodeData *nodedata_ptr)
{

	// run child

	InterrupterNode::OnEnter(nodedata_ptr);	
	
	// tiny idle

	AgentBTData &data = nodedata_ptr->GetAgentData();
	GameObject *self = data.GetGameObject();
	if (self)
	{
		self->SetSpeedStatus(TinySpeedStatus::TS_IDLE);
		SetTinySpeed(self);
	}
	TinyBlackBoard *tinybb = nodedata_ptr->GetAgentData().GetLocalBlackBoard<TinyBlackBoard>();

	//return Status::BT_RUNNING;	

	nodedata_ptr->SetChildStatus(0, Status::BT_FAILURE);

	if (tinybb->if_decorator)
		return Status::BT_RUNNING;
	else
		return Status::BT_FAILURE;

}

/*--------------------------------------------------------------------------*
Name:           OnExit

Description:    Only run when exiting the node.

Arguments:      nodedata_ptr:	current node data pointer.

Returns:        None.
*---------------------------------------------------------------------------*/
void D_If::OnExit(NodeData *nodedata_ptr)
{
	//InterrupterNode::OnExit(nodedata_ptr);
}

/*--------------------------------------------------------------------------*
Name:           OnUpdate

Description:    Run every frame.

Arguments:      dt:				delta time.
				nodedata_ptr:	current node data pointer.

Returns:        Status:			return status.
*---------------------------------------------------------------------------*/
Status D_If::OnUpdate(float dt, NodeData *nodedata_ptr)
{
	Status child_status = nodedata_ptr->GetChildStatus(0);
	TinyBlackBoard *tinybb = nodedata_ptr->GetAgentData().GetLocalBlackBoard<TinyBlackBoard>();
	
	if (child_status != Status::BT_RUNNING)
	{
		if (tinybb->if_decorator)
		{
			// resume child
			ResetChildReturnStatus(nodedata_ptr);
			RunChild(true, false, nodedata_ptr);
			return Status::BT_RUNNING;
		}
		return Status::BT_FAILURE;
	}
	else
		return child_status;


}

/*--------------------------------------------------------------------------*
Name:           OnSuspend

Description:    Only run when node is in suspended.

Arguments:      nodedata_ptr:	current node data pointer.

Returns:        Status:			return status.
*---------------------------------------------------------------------------*/
Status D_If::OnSuspend(NodeData *nodedata_ptr)
{
	return InterrupterNode::OnSuspend(nodedata_ptr);
}