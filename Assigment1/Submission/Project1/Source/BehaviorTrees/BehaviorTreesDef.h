/******************************************************************************/
/*!
\file		BehaviorTreesDef.h
\project	CS380/CS580 AI Framework
\author		Chi-Hao Kuo
\summary	All include files for the project.

Copyright (C) 2016 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
*/
/******************************************************************************/

#pragma once

#include <BehaviorTrees/BehaviorTreesShared.h>

#include <BehaviorTrees/BlackBoards/AgentAbstractData.h>
#include <BehaviorTrees/BlackBoards/NodeAbstractData.h>
#include <BehaviorTrees/BlackBoards/TinyBlackBoard.h>

#include <BehaviorTrees/AgentBehaviors.h>

#include <BehaviorTrees/Nodes/BehaviorNode.h>
#include <BehaviorTrees/TreeLogic.h>
#include <BehaviorTrees/NodeData.h>
#include <BehaviorTrees/AgentBTData.h>

#include <AIReasoner.h>
#include <BehaviorTrees/BehaviorTrees.h>

#include <BehaviorTrees/Nodes/ControlFlowNode.h>
#include <BehaviorTrees/Nodes/CompositeNode.h>
#include <BehaviorTrees/Nodes/ParallelNode.h>
#include <BehaviorTrees/Nodes/DecoratorNode.h>
#include <BehaviorTrees/Nodes/InterrupterNode.h>
#include <BehaviorTrees/Nodes/RepeaterNode.h>
#include <BehaviorTrees/Nodes/LeafNode.h>

// Add user-defined nodes definitions

/* Composite Nodes */

#include <BehaviorTrees/Nodes/ControlFlow/C_ParallelSequencer.h>
#include <BehaviorTrees/Nodes/ControlFlow/C_RandomSelector.h>
#include <BehaviorTrees/Nodes/ControlFlow/C_Sequencer.h>
#include <BehaviorTrees/Nodes/ControlFlow/C_Selector.h>

/* Decorator Nodes */

#include <BehaviorTrees/Nodes/Decorator/D_Delay.h>
#include <BehaviorTrees/Nodes/Decorator/D_Inverter.h>
#include <BehaviorTrees/Nodes/Decorator/D_If.h>
#include <BehaviorTrees/Nodes/Decorator/D_Run4Times.h>
#include <BehaviorTrees/Nodes/Decorator/D_RunOnce.h>




/* Leaf Nodes */

#include <BehaviorTrees/Nodes/Leaf/L_CheckMouseClick.h>
#include <BehaviorTrees/Nodes/Leaf/L_Idle.h>
#include <BehaviorTrees/Nodes/Leaf/L_MoveToFurthestTarget.h>
#include <BehaviorTrees/Nodes/Leaf/L_MoveToMouseTarget.h>
#include <BehaviorTrees/Nodes/Leaf/L_MoveToRandomTarget.h>
#include <BehaviorTrees/Nodes/Leaf/L_MoveToTeacherRow.h>
#include <BehaviorTrees/Nodes/Leaf/L_MoveFordward.h>
#include <BehaviorTrees/Nodes/Leaf/L_CheckLine.h>
#include <BehaviorTrees/Nodes/Leaf/L_CheckFirstInRow.h>
#include <BehaviorTrees/Nodes/Leaf/L_CheckInMiddle.h>
#include <BehaviorTrees/Nodes/Leaf/L_GoToPlayground.h>
#include <BehaviorTrees/Nodes/Leaf/L_ChoosePlayground.h>
#include <BehaviorTrees/Nodes/Leaf/L_MovePlayGroundBL.h>
#include <BehaviorTrees/Nodes/Leaf/L_MovePlayGroundBR.h>
#include <BehaviorTrees/Nodes/Leaf/L_MovePlayGroundTR.h>
#include <BehaviorTrees/Nodes/Leaf/L_MovePlayGroundTL.h>
#include <BehaviorTrees/Nodes/Leaf/L_MovePlayGroundTL.h>
#include <BehaviorTrees/Nodes/Leaf/L_PedestrianMove1.h>
#include <BehaviorTrees/Nodes/Leaf/L_PedestrianMove2.h>
#include <BehaviorTrees/Nodes/Leaf/L_PedestrianMove3.h>
#include <BehaviorTrees/Nodes/Leaf/L_PedestrianMove4.h>
#include <BehaviorTrees/Nodes/Leaf/L_PedestrianMove5.h>
#include <BehaviorTrees/Nodes/Leaf/L_PedestrianMove6.h>
#include <BehaviorTrees/Nodes/Leaf/L_PedestrianMove7.h>
#include <BehaviorTrees/Nodes/Leaf/L_PedestrianMove8.h>
#include <BehaviorTrees/Nodes/Leaf/L_PedestrianMove9.h>
#include <BehaviorTrees/Nodes/Leaf/L_PedestrianMove10.h>
#include <BehaviorTrees/Nodes/Leaf/L_PedestrianMove11.h>
#include <BehaviorTrees/Nodes/Leaf/L_PedestrianMove12.h>
#include <BehaviorTrees/Nodes/Leaf/L_MoveToIceCreamSpot.h>
#include <BehaviorTrees/Nodes/Leaf/L_SellingIcecreams.h>
#include <BehaviorTrees/Nodes/Leaf/L_BuyingIcecreams.h>
#include <BehaviorTrees/Nodes/Leaf/L_LeaveIceCreamVan.h>
#include <BehaviorTrees/Nodes/Leaf/L_ShrinkIceCreamVan.h>











