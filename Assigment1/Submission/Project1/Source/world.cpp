/* Copyright Steve Rabin, 2013. 
 * All rights reserved worldwide.
 *
 * This software is provided "as is" without express or implied
 * warranties. You may freely copy and compile this source into
 * applications you distribute provided that the copyright text
 * below is included in the resulting source code, for example:
 * "Portions Copyright Steve Rabin, 2013"
 */

#include <Stdafx.h>

//#define UNIT_TESTING

//vector for the row
std::vector<GameObject *> row_vec;
int playgrounds[4];
int ice_cream_left = 4;


World::World(void)
: m_initialized(false)
{

}

World::~World(void)
{
}

void World::InitializeSingletons( void )
{
	//Create Singletons
	m_clock = &g_clock;
	m_database = &g_database;
	m_msgroute = &g_msgroute;
	m_debuglog = &g_debuglog;
	m_random = &g_random;
}

void World::Initialize( CMultiAnim *pMA, std::vector< CTiny* > *pv_pChars, CSoundManager *pSM, double dTimeCurrent )
{
	if(!m_initialized)
	{
		m_initialized = true;
	

#ifdef UNIT_TESTING

		//Create unit test game objects
		GameObject* unittest1 = new GameObject( g_database.GetNewObjectID(), OBJECT_Ignore_Type, "UnitTest1" );
		GameObject* unittest2 = new GameObject( g_database.GetNewObjectID(), OBJECT_Ignore_Type, "UnitTest2" );
		GameObject* unittest3a = new GameObject( g_database.GetNewObjectID(), OBJECT_Ignore_Type, "UnitTest3a" );
		GameObject* unittest3b = new GameObject( g_database.GetNewObjectID(), OBJECT_Ignore_Type, "UnitTest3b" );
		GameObject* unittest4 = new GameObject( g_database.GetNewObjectID(), OBJECT_Ignore_Type, "UnitTest4" );
		GameObject* unittest5 = new GameObject( g_database.GetNewObjectID(), OBJECT_Ignore_Type, "UnitTest5" );
		GameObject* unittest6 = new GameObject( g_database.GetNewObjectID(), OBJECT_Ignore_Type, "UnitTest6" );
		
		D3DXVECTOR3 pos(0.0f, 0.0f, 0.0f);
		unittest1->CreateBody( 100, pos );
		unittest2->CreateBody( 100, pos );
		unittest3a->CreateBody( 100, pos );
		unittest3b->CreateBody( 100, pos );
		unittest4->CreateBody( 100, pos );
		unittest5->CreateBody( 100, pos );
		unittest6->CreateBody( 100, pos );

		unittest1->CreateStateMachineManager();
		unittest2->CreateStateMachineManager();
		unittest3a->CreateStateMachineManager();
		unittest3b->CreateStateMachineManager();
		unittest4->CreateStateMachineManager();
		unittest5->CreateStateMachineManager();
		unittest6->CreateStateMachineManager();
		
		g_database.Store( *unittest1 );
		g_database.Store( *unittest2 );
		g_database.Store( *unittest3a );
		g_database.Store( *unittest3b );
		g_database.Store( *unittest4 );
		g_database.Store( *unittest5 );
		g_database.Store( *unittest6 );

		//Give the unit test game objects a state machine
		unittest1->GetStateMachineManager()->PushStateMachine( *new UnitTest1( *unittest1 ), STATE_MACHINE_QUEUE_0, TRUE );
		unittest2->GetStateMachineManager()->PushStateMachine( *new UnitTest2a( *unittest2 ), STATE_MACHINE_QUEUE_0, TRUE );
		unittest3a->GetStateMachineManager()->PushStateMachine( *new UnitTest3a( *unittest3a ), STATE_MACHINE_QUEUE_0, TRUE );
		unittest3b->GetStateMachineManager()->PushStateMachine( *new UnitTest3b( *unittest3b ), STATE_MACHINE_QUEUE_0, TRUE );
		unittest4->GetStateMachineManager()->PushStateMachine( *new UnitTest4( *unittest4 ), STATE_MACHINE_QUEUE_0, TRUE );
		unittest5->GetStateMachineManager()->PushStateMachine( *new UnitTest5( *unittest5 ), STATE_MACHINE_QUEUE_0, TRUE );
		unittest6->GetStateMachineManager()->PushStateMachine( *new UnitTest6( *unittest6 ), STATE_MACHINE_QUEUE_0, TRUE );

#else
		g_trees.Initialize();		// initialize behavior tree

	//init playgrounds
	for (unsigned i = 0; i < 4; i++)
		playgrounds[i] = -1;
	
	//teacher creation 
	char teacher_name[10] = "Teacher";
	//sprintf(teacher_name, "%s%d", teacher_name, 0);
	GameObject* npc = new GameObject(g_database.GetNewObjectID(), OBJECT_NPC, teacher_name);
	D3DXVECTOR3 pos(0.0f, 0.0f, 0.0f);
	pos.x = -0.5;
	pos.z = 0.5;
	npc->CreateBody(100, pos);
	npc->CreateMovement();
	npc->CreateTiny(pMA, pv_pChars, pSM, dTimeCurrent, 1.0f, 0.5f, 0.5f);	//Color if needed
	npc->GetBody().SetScale(npc->GetBody().GetScale() * 2);
	g_database.Store(*npc);

	g_trees.Register(teacher_name, "Teacher");							// register agent to behavior tree
	g_trees.GetAgentData(teacher_name).InitialTinyBlackBoard(npc);		// initialize local blackboard for each tiny
	auto tiny_blackboard = g_trees.GetAgentData(teacher_name).GetTinyBlackBoard();
	tiny_blackboard->row_vec = &row_vec;
	tiny_blackboard->playgrounds = playgrounds;
	tiny_blackboard->ice_cream_left = &ice_cream_left;



	for( int i=1; i<5; i++ )
	{
		//Create game objects
		char name[10] = "Child";
		sprintf( name, "%s%d", name, i );
		GameObject* npc = new GameObject( g_database.GetNewObjectID(), OBJECT_NPC, name );
		D3DXVECTOR3 pos(0.0f, 0.0f, 0.0f);
		pos.x = g_random.RangeFloat(-1.5,1);
		pos.z = g_random.RangeFloat();
		npc->CreateBody( 100, pos );
		npc->CreateMovement();
		npc->CreateTiny( pMA, pv_pChars, pSM, dTimeCurrent, 0.5f, 1.0f, 1.0f );	//Color if needed
		g_database.Store( *npc );

		g_trees.Register(name, "TreeChilds");							// register agent to behavior tree
		g_trees.GetAgentData(name).InitialTinyBlackBoard(npc);		// initialize local blackboard for each tiny
	
		tiny_blackboard = g_trees.GetAgentData(name).GetTinyBlackBoard();
		tiny_blackboard->row_vec = &row_vec;
		tiny_blackboard->playgrounds = playgrounds;
		tiny_blackboard->ice_cream_left = &ice_cream_left;

	}
	//teacher creation 
	char pedestrian[20] = "pedestrian";
	npc = new GameObject(g_database.GetNewObjectID(), OBJECT_NPC, pedestrian);	
	pos.x = 0.42f;
	pos.z = 1.08f;
	npc->CreateBody(100, pos);
	npc->CreateMovement();
	npc->CreateTiny(pMA, pv_pChars, pSM, dTimeCurrent, 1.0f, 0.5f, 0.5f);	//Color if needed
	npc->GetBody().SetScale(npc->GetBody().GetScale() * 0.5f);
	g_database.Store(*npc);

	g_trees.Register(pedestrian, "Pedestrian");					// register agent to behavior tree
	g_trees.GetAgentData(pedestrian).InitialTinyBlackBoard(npc);		// initialize local blackboard for each tiny
	tiny_blackboard = g_trees.GetAgentData(pedestrian).GetTinyBlackBoard();
	tiny_blackboard->row_vec = &row_vec;
	tiny_blackboard->playgrounds = playgrounds;
	tiny_blackboard->ice_cream_left = &ice_cream_left;


	//ice van creation
	char icecream_van[20] = "icecream_van";
	npc = new GameObject(g_database.GetNewObjectID(), OBJECT_NPC, icecream_van);
	pos.x = 0.5;
	pos.z = 1;
	npc->CreateBody(100, pos);
	npc->CreateMovement();
	npc->CreateTiny(pMA, pv_pChars, pSM, dTimeCurrent, 0.1f, 0.1f, 1.0f);	//Color if needed
	npc->GetBody().SetScale(npc->GetBody().GetScale() * 1.5f);
	g_database.Store(*npc);

	g_trees.Register(icecream_van, "Icecream_van");					// register agent to behavior tree
	g_trees.GetAgentData(icecream_van).InitialTinyBlackBoard(npc);		// initialize local blackboard for each tiny
	tiny_blackboard = g_trees.GetAgentData(icecream_van).GetTinyBlackBoard();
	tiny_blackboard->row_vec = &row_vec;
	tiny_blackboard->playgrounds = playgrounds;
	tiny_blackboard->ice_cream_left = &ice_cream_left;

#endif

	}
}


void World::PostInitialize()
{
	g_database.Initialize();
}


void World::Update(float dt)
{
	g_clock.MarkTimeThisTick();
	g_database.Update();
	g_trees.Update(dt);
}

void World::Animate( double dTimeDelta )
{
	g_database.Animate( dTimeDelta );
}

void World::AdvanceTimeAndDraw( IDirect3DDevice9* pd3dDevice, D3DXMATRIX* pViewProj, double dTimeDelta, D3DXVECTOR3 *pvEye )
{
	g_database.AdvanceTimeAndDraw( pd3dDevice, pViewProj, dTimeDelta, pvEye );
}

void World::RestoreDeviceObjects( LPDIRECT3DDEVICE9 pd3dDevice )
{
	return( g_database.RestoreDeviceObjects( pd3dDevice ) );
}

void World::InvalidateDeviceObjects( void )
{
	g_database.InvalidateDeviceObjects();
}
