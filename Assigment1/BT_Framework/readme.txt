Student Name:Ivan Iglesias

Project Name:TeacherWithChildren

What I implemented:

The system consists on 7 npc where 4 of them will be childs controlled by the same tree(the green ones).
At the begining the childs will spawn radomly and will seek the teacher making a row after her.
Whenever all the children are behind her she will start moving to the town and the children will follow her.

On the town the children will divide themselves into the "playgrounds/houses" based on how fast they arrived.
By this time the icecream van will already be in the town and the teacher will go to buy 4 icecreams for the children.

After buying the icecreams the van will run out of icecream. To notice this the van shrinks and gets away of the town.
The teacher will seek the childs and give them the icecream. After that, she will also run away of the town.

The npc that is left will be a pedestrian walking through the streets of the town. 

Directions (if needed):

What I liked about the project and framework:

Framework
	I liked that there was a visual feedback on what I was doing. With animations on the models that
	realistically show how fast the npc was moving. 
	I also liked that the access to the scale, color, speed modes(walk, jog, idle) was easy.
Project
	Although I had a hard time knowing what I wanted to do. I liked to be exposed to this situation.
	I finish the project wanting to redo it with what I know now. 

What I disliked about the project and framework:

Framework
	I disliked the first contant with it and how the memory owned by each agent and nodes are managed.
	I started to fully understand how the iteration between nodes was working when I was already finishing
	the assigment.
	
	Although now I understand the framework better I wouldn't like to make another project with it.
Project
	It was hard to know what was the line between something iteresting and something that it is not.
	I understand the freedom that we own on this assigment but still will want a bit more of controll 
	apart from the requirements. 
	
Any difficulties I experienced while doing the project:

The hardest part was not know what to do. I couldn't think of something that will just meet the requirements
so I just went adding nodes as I realise I needed more. I think that my biggest error was to start the assigment
without thinking on 4 npcs with 4 different trees. I felt at some point that I was filling the gaps to meet 
the requirements.

For sure that if I start the assigment again I would be more aware of what I am facing and will start planning
something more complex and interesting staff instead of start coding the first thing that comes to my mind. 

Finally, I added to much basic nodes and didn't have enough time to edit all the headers of the files/function.

Hours spent:
21 hours

New selector node (name):

New decorator nodes (names):

D_If
D_RunOnce
D_Run4Times

10 total nodes (names):

L_MoveFordward
L_MoveToTeacherRow
L_ChoosePlayground
L_GoToPlayground
L_MoveFordward
L_CheckLine
D_If
D_RunOnce
D_Run4Times
L_MoveToIceCreamSpot

4 Behavior trees (names):

Pedestrian
TreeChilds
Teacher
Icecream_van

Extra credit: