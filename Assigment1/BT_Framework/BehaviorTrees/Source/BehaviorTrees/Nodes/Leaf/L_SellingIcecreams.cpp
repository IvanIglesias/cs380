/******************************************************************************/
/*!
\file		L_MoveToTeacherRow.cpp
\project	CS380/CS580 AI Framework
\author		Chi-Hao Kuo
\summary	Action: Move to a random point.

Copyright (C) 2016 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
*/
/******************************************************************************/

#include <Stdafx.h>

using namespace BT;


/*--------------------------------------------------------------------------*
Name:           GetLocalBlackBoard

Description:    Get custom data pointer.

Arguments:      None.

Returns:        D_DelayData*:	custom node data pointer.
*---------------------------------------------------------------------------*/
L_SellingIcecreamsData *L_SellingIcecreams::GetLocalBlackBoard(NodeData *nodedata_ptr)
{
	return nodedata_ptr->GetLocalBlackBoard<L_SellingIcecreamsData>();
}

/*--------------------------------------------------------------------------*
Name:           InitialLocalBlackBoard

Description:    Initial custom data.

Arguments:      None.

Returns:        None.
*---------------------------------------------------------------------------*/
void L_SellingIcecreams::InitialLocalBlackBoard(NodeData *nodedata_ptr)
{
	nodedata_ptr->InitialLocalBlackBoard<L_SellingIcecreamsData>();
}


/*--------------------------------------------------------------------------*
Name:           OnInitial

Description:    Only run when initializing the node.

Arguments:      nodedata_ptr:	current node data pointer.

Returns:        None.
*---------------------------------------------------------------------------*/
void L_SellingIcecreams::OnInitial(NodeData *nodedata_ptr)
{
	LeafNode::OnInitial(nodedata_ptr);
	InitialLocalBlackBoard(nodedata_ptr);

}

/*--------------------------------------------------------------------------*
Name:           OnEnter

Description:    Only run when entering the node.

Arguments:      nodedata_ptr:	current node data pointer.

Returns:        Status:			return status.
*---------------------------------------------------------------------------*/
Status L_SellingIcecreams::OnEnter(NodeData *nodedata_ptr)
{
	LeafNode::OnEnter(nodedata_ptr);

	TinyBlackBoard *tinybb = nodedata_ptr->GetAgentData().GetLocalBlackBoard<TinyBlackBoard>();
	GameObject *self = nodedata_ptr->GetAgentData().GetGameObject();
	//all the children in the line
	auto local_black = GetLocalBlackBoard(nodedata_ptr);

	if(*tinybb->ice_cream_left < 0)
		return Status::BT_FAILURE;

	if (*tinybb->ice_cream_left != local_black->prev_ice_cream_num)
	{
		*tinybb->ice_cream_left = local_black->prev_ice_cream_num;
		return Status::BT_SUCCESS;
	}
	return Status::BT_RUNNING;

}

/*--------------------------------------------------------------------------*
Name:           OnExit

Description:    Only run when exiting the node.

Arguments:      nodedata_ptr:	current node data pointer.

Returns:        None.
*---------------------------------------------------------------------------*/
void L_SellingIcecreams::OnExit(NodeData *nodedata_ptr)
{
	LeafNode::OnExit(nodedata_ptr);
}

/*--------------------------------------------------------------------------*
Name:           OnUpdate

Description:    Run every frame.

Arguments:      dt:				delta time.
nodedata_ptr:	current node data pointer.

Returns:        Status:			return status.
*---------------------------------------------------------------------------*/
Status L_SellingIcecreams::OnUpdate(float dt, NodeData *nodedata_ptr)
{
	LeafNode::OnUpdate(dt, nodedata_ptr);

	TinyBlackBoard *tinybb = nodedata_ptr->GetAgentData().GetLocalBlackBoard<TinyBlackBoard>();
	GameObject *self = nodedata_ptr->GetAgentData().GetGameObject();
	//all the children in the line
	auto local_black = GetLocalBlackBoard(nodedata_ptr);

	if (*tinybb->ice_cream_left < 0)
		return Status::BT_FAILURE;

	if (*tinybb->ice_cream_left != local_black->prev_ice_cream_num)
	{
		*tinybb->ice_cream_left = local_black->prev_ice_cream_num;
		return Status::BT_SUCCESS;
	}
	return Status::BT_RUNNING;
}

/*--------------------------------------------------------------------------*
Name:           OnSuspend

Description:    Only run when node is in suspended.

Arguments:      nodedata_ptr:	current node data pointer.

Returns:        Status:			return status.
*---------------------------------------------------------------------------*/
Status L_SellingIcecreams::OnSuspend(NodeData *nodedata_ptr)
{
	return LeafNode::OnSuspend(nodedata_ptr);
}