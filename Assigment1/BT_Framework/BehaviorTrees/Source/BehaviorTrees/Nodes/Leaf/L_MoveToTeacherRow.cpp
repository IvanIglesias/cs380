/******************************************************************************/
/*!
\file		L_MoveToTeacherRow.cpp
\project	CS380/CS580 AI Framework
\author		Chi-Hao Kuo
\summary	Action: Move to a random point.

Copyright (C) 2016 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
*/
/******************************************************************************/

#include <Stdafx.h>

using namespace BT;

/* protected methods */


/*--------------------------------------------------------------------------*
Name:           GetLocalBlackBoard

Description:    Get custom data pointer.

Arguments:      None.

Returns:        D_DelayData*:	custom node data pointer.
*---------------------------------------------------------------------------*/
L_MoveToTeacherData *L_MoveToTeacherRow::GetLocalBlackBoard(NodeData *nodedata_ptr)
{
	return nodedata_ptr->GetLocalBlackBoard<L_MoveToTeacherData>();
}

void L_MoveToTeacherRow::InitialLocalBlackBoard(NodeData *nodedata_ptr)
{
	nodedata_ptr->InitialLocalBlackBoard<L_MoveToTeacherData>();
}



/*--------------------------------------------------------------------------*
Name:           OnInitial

Description:    Only run when initializing the node.

Arguments:      nodedata_ptr:	current node data pointer.

Returns:        None.
*---------------------------------------------------------------------------*/
void L_MoveToTeacherRow::OnInitial(NodeData *nodedata_ptr)
{
	LeafNode::OnInitial(nodedata_ptr);
	InitialLocalBlackBoard(nodedata_ptr);
}

/*--------------------------------------------------------------------------*
Name:           OnEnter

Description:    Only run when entering the node.

Arguments:      nodedata_ptr:	current node data pointer.

Returns:        Status:			return status.
*---------------------------------------------------------------------------*/
Status L_MoveToTeacherRow::OnEnter(NodeData *nodedata_ptr)
{
	LeafNode::OnEnter(nodedata_ptr);

	//if not other child arrived yet
	GameObject *self = nodedata_ptr->GetAgentData().GetGameObject();
	GameObject *first;
	first = GetFirstAgent();
	auto local_data = GetLocalBlackBoard(nodedata_ptr);

	

	if (local_data->in_line)
		return Status::BT_FAILURE;
	
	if (first)
	{
		auto opposite_pos = first->GetBody().GetPos() - (first->GetBody().GetDir() * 0.1f);
		self->SetTargetPOS(opposite_pos);
		self->SetSpeedStatus(TinySpeedStatus::TS_JOG);
		SetTinySpeed(self);

		return Status::BT_RUNNING;
	}
	else
		return Status::BT_FAILURE;

}

/*--------------------------------------------------------------------------*
Name:           OnExit

Description:    Only run when exiting the node.

Arguments:      nodedata_ptr:	current node data pointer.

Returns:        None.
*---------------------------------------------------------------------------*/
void L_MoveToTeacherRow::OnExit(NodeData *nodedata_ptr)
{
	LeafNode::OnExit(nodedata_ptr);
}

/*--------------------------------------------------------------------------*
Name:           OnUpdate

Description:    Run every frame.

Arguments:      dt:				delta time.
nodedata_ptr:	current node data pointer.

Returns:        Status:			return status.
*---------------------------------------------------------------------------*/
Status L_MoveToTeacherRow::OnUpdate(float dt, NodeData *nodedata_ptr)
{
	LeafNode::OnUpdate(dt, nodedata_ptr);

	GameObject *self = nodedata_ptr->GetAgentData().GetGameObject();
	GameObject *first;
	first = GetFirstAgent();
	auto local_data = GetLocalBlackBoard(nodedata_ptr);
	TinyBlackBoard *tinybb = nodedata_ptr->GetAgentData().GetLocalBlackBoard<TinyBlackBoard>();

	if(local_data->in_line)
		return Status::BT_FAILURE;

	//if already one in line recalculate the position with the last one
	if (!tinybb->row_vec->empty())
	{
		auto opposite_pos = first->GetBody().GetPos() - (first->GetBody().GetDir() * (0.1f *(tinybb->row_vec->size() + 1)));
		self->SetTargetPOS(opposite_pos);
		self->SetSpeedStatus(TinySpeedStatus::TS_JOG);
		SetTinySpeed(self);
	}	
	if (IsNear(self->GetBody().GetPos(), self->GetTargetPOS()))
	{		
		tinybb->row_vec->push_back(self);
		local_data->in_line = true;
		return Status::BT_SUCCESS;
	}
	return Status::BT_RUNNING;
}

/*--------------------------------------------------------------------------*
Name:           OnSuspend

Description:    Only run when node is in suspended.

Arguments:      nodedata_ptr:	current node data pointer.

Returns:        Status:			return status.
*---------------------------------------------------------------------------*/
Status L_MoveToTeacherRow::OnSuspend(NodeData *nodedata_ptr)
{
	return LeafNode::OnSuspend(nodedata_ptr);
}