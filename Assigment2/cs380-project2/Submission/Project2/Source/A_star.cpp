/******************************************************************************/
/*!
\file		A_star.cpp
\project	CS380 Project 2
\author		Ivan Iglesias
\summary	Algorithm of A*

Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
*/
/******************************************************************************/
#include <Stdafx.h>
#include <stack>

//Deletes all the memmory stored in the grids
AStar::~AStar()
{
	for (int i = 0; i < width; ++i) {
		delete[] grid_info[i];
	}
	delete[] grid_info;

	for (int i = 0; i < width; ++i) {
		delete[] close_list[i];
	}
	delete[] close_list;
}
//Checks if the position is valid
bool AStar::CheckPos(int row, int col)
{	
	if((row >= 0) && (row < width) &&
		(col >= 0) && (col < width))
		return true;
	else 
		return false;
}
//Checks if the position is the destination
bool AStar::LastPosition(int row, int col)
{
	if (row == dest.first && col == dest.second)
		return true;
	else
		return false;
}
//Trace the path from the nodes that have a parent that
//lead to another node
void AStar::ComputeResultingPath(WaypointList & m_waypointList)
{
	//Use a stack to order the positions of the path
	std::stack<grid_pos> Path;

	int row = dest.first;
	int col = dest.second;

	//Iterate through the array to move from parent to child
	//store this path in the stack
	while (!(grid_info[row][col].parent_row == row
		&& grid_info[row][col].parent_col == col))
	{
		//add the path nodes to the stack 
		Path.push(std::make_pair(row, col));
		//move to the next node
		int temp_row = grid_info[row][col].parent_row;
		int temp_col = grid_info[row][col].parent_col;
		row = temp_row;
		col = temp_col;
	}

	//pop the values from the stack and insert them in the waypoint list
	Path.push(std::make_pair(row, col));
	while (!Path.empty())
	{
		std::pair<int, int> p = Path.top();
		Path.pop();
		//Conver the values into world position and push them into waypoints
		D3DXVECTOR3 spot = g_terrain.GetCoordinates(p.first, p.second);
		m_waypointList.push_back(spot);		
	}
	//compute rubberbanding
	if (rubberbanding && m_waypointList.size() > 2)
	{
		auto it_start = m_waypointList.begin();
		auto it_mid = it_start;
		it_mid++;
		auto it_last = it_mid;
		it_last++;

		//take two point and check if a straight line can be made
		//between them
		while (it_last != m_waypointList.end())
		{
			auto point1 = *it_start;
			auto point2 = *it_last;			
			int row1, row2, col1, col2;			
			g_terrain.GetRowColumn(&point1, &row1, &col1);
			g_terrain.GetRowColumn(&point2, &row2, &col2);

			if (IsStraightLine(row1, col1, row2, col2))
			{
				it_mid = m_waypointList.erase(it_mid);
			}
			else
			{
				it_start = it_mid;
				it_mid = it_last;
			}
				it_last++;
		}

	}

	//Smoothing
	if (catmull)
	{
		auto it_start = m_waypointList.begin();
		auto it_last = it_start;
		it_last++;
		const float size_max = 1.5f / width;
		//Generate more points if distance between them
		//is more than 1.5
		while (it_last != m_waypointList.end())
		{
			auto point1 = *it_start;
			auto point2 = *it_last;
			auto vector = point1 - point2;
			auto length = D3DXVec3Length(&vector);
			
			if (length > size_max)
			{
				auto midpoint = (point1 + point2) * 0.5;
				it_last = m_waypointList.insert(it_last, midpoint);
			}
			else
			{
				it_start = it_last;
				it_last++;
			}
		}

		if (m_waypointList.size() < 2)
			return;
		
		//apply smothing to this points 
		auto it_point0 = m_waypointList.begin();
		auto it_point1 = it_point0;
		it_point1++;
		//special case 2 points only
		if (m_waypointList.size() == 2)
		{
			return InsertSmothing(it_point0, it_point0, it_point1, it_point1, m_waypointList);
		}

		auto it_point2 = it_point1;
		it_point2++;
		auto it_point3 = it_point2;
		it_point3++;

		//first points
		InsertSmothing(it_point0, it_point0, it_point1, it_point2, m_waypointList);

		while (1)
		{
			if (it_point3 == m_waypointList.end())
			{
				InsertSmothing(it_point0, it_point1, it_point2, it_point2, m_waypointList);
				break;
			}
			InsertSmothing(it_point0, it_point1, it_point2, it_point3, m_waypointList);			
			it_point0 = it_point1;
			it_point1 = it_point2;
			it_point2 = it_point3;
			it_point3++;	
		}			
	}
	return;
}
//Checks if neighbour is valid and inserts it into the openlist
bool AStar::CheckNeighbour(int original_r, int original_c, WaypointList & m_waypointList, neighbours position)
{
	int row = 0;
	int col = 0;

	//set the position to the row and column
	switch (position)
	{
	case top:
		row = original_r - 1;
		col = original_c;
		break;
	case bot:
		row = original_r + 1;
		col = original_c;
		break;
	case right:
		row = original_r;
		col = original_c + 1;
		break;
	case left:
		row = original_r;
		col = original_c - 1;
		break;
	case top_left:
		row = original_r - 1;
		col = original_c - 1;
		break;
	case top_right:
		row = original_r - 1;
		col = original_c + 1;
		break;
	case bot_left:
		row = original_r + 1;
		col = original_c - 1;
		break;
	case bot_right:
		row = original_r + 1;
		col = original_c + 1;
		break;
	default:
		break;
	}	
	//check if cell is valid
	if (CheckPos(row, col) == true)
	{
		//check if this is the last cell
		if (LastPosition(row, col) == true)
		{
			//check if we can move to this cell
			if (!IsStraightLine(row, col, original_r, original_c))
				return false;
			//Set the parent of this last cell to the previous one
			grid_info[row][col].parent_row = original_r;
			grid_info[row][col].parent_col = original_c;
			//we finish so we compute the path
			ComputeResultingPath(m_waypointList);
			return true;
		}
		//Check if it is already in the close list
		//Check also if it is a wall or not
		//Otherwise is a valid point
		else if (!close_list[row][col] && !g_terrain.IsWall(row, col))
		{
			//Check that we can move to this point
			if (!IsStraightLine(row, col, original_r, original_c))
				return false;

			float f_new, g_new, h_new;

			//check what kind of movement is to apply a different cost
			if(position == top_left || position == top_right || position == bot_left || position == bot_right)
				g_new = grid_info[original_r][original_c].g_value + sqrt(2.0f);
			else
				g_new = grid_info[original_r][original_c].g_value + 1;
			//Compute the new heuristics value
			h_new = CalculateHeuristics(row, col);
			f_new = g_new + (h_new * heuristic_weight);
			
			//Compare the values with the previous one
			//If new value is smaller we insert it in the 
			//open list
			if (grid_info[row][col].f_value == FLT_MAX || grid_info[row][col].f_value > f_new)
			{
				open_list.insert(std::make_pair(f_new, std::make_pair(row, col)));

				//Paint the grid position
				g_terrain.SetColor(row, col, DEBUG_COLOR_BLUE);

				//Set the parents 
				grid_info[row][col].parent_row = original_r;
				grid_info[row][col].parent_col = original_c;
				//Update the heuristics value
				grid_info[row][col].f_value = f_new;
				grid_info[row][col].g_value = g_new;
				grid_info[row][col].h_value = h_new;
			}
		}
	}
	return false;
}
//Clears all the data of the containers
void AStar::ClearData()
{	
	for (int i = 0; i<width; i++)
	{
		for (int j = 0; j<width; j++)
		{
			grid_info[i][j].f_value = FLT_MAX;
			grid_info[i][j].g_value = FLT_MAX;
			grid_info[i][j].h_value = FLT_MAX;
			grid_info[i][j].parent_row = -1;
			grid_info[i][j].parent_col = -1;
			close_list[i][j] = false;
		}
	}
	open_list.clear();
}
//Computes the CatmullRom smoothing with the directX function given some points
void AStar::InsertSmothing(std::list<D3DXVECTOR3>::iterator point0, std::list<D3DXVECTOR3>::iterator point1,
	std::list<D3DXVECTOR3>::iterator point2, std::list<D3DXVECTOR3>::iterator point3, WaypointList & m_waypointList)
{
	D3DXVECTOR3 vec1, vec2, vec3;
	D3DXVec3CatmullRom(&vec1, &*point0, &*point1, &*point2, &*point3, 0.25);
	D3DXVec3CatmullRom(&vec2, &*point0, &*point1, &*point2, &*point3, 0.50);
	D3DXVec3CatmullRom(&vec3, &*point0, &*point1, &*point2, &*point3, 0.75);

	m_waypointList.insert(point2, vec1);
	m_waypointList.insert(point2, vec2);
	m_waypointList.insert(point2, vec3);
}
//Compute the new heuristics value
float AStar::CalculateHeuristics(int row, int col)
{

	auto diff_x = static_cast<float>(abs(row - dest.first));
	auto diff_y = static_cast<float>(abs(col - dest.second));

	switch (heuristic_option)		
	{
	case 0:	 //Euclidean
		return sqrtf(diff_x*diff_x + diff_y*diff_y);
		break;
	case 1:  //Octile
		return min(diff_x, diff_y) * sqrt(2.0f) + max(diff_x, diff_y) - min(diff_x, diff_y);
		break;
	case 2:	//Chebyshev
		return max(diff_x, diff_y);
		break;
	case 3: //Manhattan
		return diff_x + diff_y;
		break;
	default:
		//Euclidean
		return sqrtf(diff_x*diff_x + diff_y*diff_y);
		break;
	}	
}
//Creates a new board
void AStar::NewBoard(size_t board_width)
{
	ClearData();
	//Deletes the previous one
	if (board_width != width)
	{
		for (int i = 0; i < width; ++i) {
			delete[] grid_info[i];
		}
		delete[] grid_info;

		for (int i = 0; i < width; ++i) {
			delete[] close_list[i];
		}
		delete[] close_list;
		//allocates new ones
		width = board_width;
		grid_info = new grid_cell*[board_width];
		for (unsigned i = 0; i < board_width; i++)
			grid_info[i] = new grid_cell[board_width];

		close_list = new bool*[board_width];
		for (unsigned i = 0; i < board_width; i++)
			close_list[i] = new bool[board_width];

		//set the initial values
		for (unsigned i = 0; i < board_width; i++)
		{
			for (unsigned j = 0; j < board_width; j++)
			{
				grid_info[i][j].f_value = FLT_MAX;
				grid_info[i][j].g_value = FLT_MAX;
				grid_info[i][j].h_value = FLT_MAX;
				grid_info[i][j].parent_row = -1;
				grid_info[i][j].parent_col = -1;
				close_list[i][j] = false;
			}
		}
	}
}
//Sets the given weight
void AStar::SetHeuristicWeight(float weight)
{
	heuristic_weight = weight;
}
//Set the heuristic option
void AStar::SetHeuristicOption(int heuris_option)
{
	heuristic_option = heuris_option;
}
//Sets if rubberbanding is active
void AStar::SetRubberbanding(bool rubberbanding_flag)
{
	rubberbanding = rubberbanding_flag;
}
//Sets if catmull is active
void AStar::SetCatmull(bool catmull_flag)
{
	catmull = catmull_flag;
}
//Checks if a straightline can be computed between two points
bool AStar::IsStraightLine(int r, int c, int curR, int curC)
{
	//take the smallest and bigger values 
	int min_row, max_row, min_col, max_col;

	min_row = (r > curR) ? curR : r;
	max_row = (r > curR) ? r : curR;
	min_col = (c > curC) ? curC : c;
	max_col = (c > curC) ? c : curC;

	//check if walls inside the rectangle
	for (int i = min_row; i <= max_row; i++)
	{
		for (int j = min_col; j <= max_col; j++)
		{
			if (g_terrain.IsWall(i, j))
				return false;
		}
	}
	return true;
}
//Init the contalizer to the default values
void AStar::InitializeContainers()
{
	int i, j;

	// Initialising the parameters of the starting node 
	i = src.first, j = src.second;
	grid_info[i][j].f_value = 0.0;
	grid_info[i][j].g_value = 0.0;
	grid_info[i][j].h_value = 0.0;
	grid_info[i][j].parent_row = i;
	grid_info[i][j].parent_col = j;
	//Inserting the first value for the iteration
	open_list.insert(std::make_pair(0.0f, std::make_pair(i, j)));

}
//Main loop of the A_Star algorithm
bool AStar::LoopA_Star(WaypointList & m_waypointList)
{
	//while there is something to check on the open_list
	while (!open_list.empty())
	{
		//grab the value with less cost
		cost_grid_pos p = *open_list.begin();

		//delete the taken one from the openlist
		open_list.erase(open_list.begin());

		//take the row and column values
		int row = p.second.first;
		int col = p.second.second;
		// Add this vertex to the closed list 
		close_list[row][col] = true;
		//set the color
		g_terrain.SetColor(row, col, DEBUG_COLOR_YELLOW);

		//check all the neighbours
		if (CheckNeighbour(row, col, m_waypointList, top))
			return true;		

		if (CheckNeighbour(row, col, m_waypointList, bot))
			return true;
	
		if (CheckNeighbour(row, col, m_waypointList, right))
			return true;		

		if (CheckNeighbour(row, col, m_waypointList, left))
			return true;

		if (CheckNeighbour(row, col, m_waypointList, top_right))
			return true;	

		if (CheckNeighbour(row, col, m_waypointList, top_left))
			return true;

		if (CheckNeighbour(row, col, m_waypointList, bot_right))
			return true;
	
		if (CheckNeighbour(row, col, m_waypointList, bot_left))
			return true;

		//If single step is active then we will return here
		if (single_step)
			return false;
	}
	return false;
}
